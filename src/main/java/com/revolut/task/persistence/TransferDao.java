package com.revolut.task.persistence;

import com.revolut.task.model.Transaction;
import org.hibernate.Session;

public class TransferDao extends SessionDao {

  public Transaction saveTransaction(Transaction transaction) {
    Session session = getCurrentSession();
    transaction = (Transaction) session.merge(transaction);
    session.getTransaction().commit();
    return transaction;
  }

}
