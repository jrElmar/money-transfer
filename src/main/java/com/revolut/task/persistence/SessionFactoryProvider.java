package com.revolut.task.persistence;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public final class SessionFactoryProvider {

  public static SessionFactory getSessionFactory() {
    return SessionFactoryHolder.SESSION_FACTORY;
  }

  private static class SessionFactoryHolder {

    private static final SessionFactory SESSION_FACTORY = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
      StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
          .configure()
          .build();
      try {
        return new MetadataSources(registry).buildMetadata()
            .buildSessionFactory();
      } catch (RuntimeException e) {
        if (registry != null) {
          StandardServiceRegistryBuilder.destroy(registry);
        }
        throw e;
      }
    }
  }
}
