package com.revolut.task.persistence;

import com.revolut.task.model.Customer;
import org.hibernate.Session;

import java.util.Optional;

public class CustomerDao extends SessionDao {

  public Optional<Customer> findCustomerById(long id) {
    Session session = getCurrentSession();
    Customer customer = session.find(Customer.class, id);
    return Optional.ofNullable(customer);
  }

  public Customer saveCustomer(Customer customer) {
    Session session = getCurrentSession();
    customer = (Customer) session.merge(customer);
    session.getTransaction().commit();
    return customer;
  }
}
