package com.revolut.task.persistence;

import com.revolut.task.model.Account;
import org.hibernate.Session;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;



public class AccountDao extends SessionDao {


  public Optional<Account> findByAccountId(String accountId) {
    try {
      Session session = getCurrentSession();
      CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
      CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
      Root<Account> root = criteriaQuery.from(Account.class);
      criteriaQuery.select(root)
          .where(criteriaBuilder.equal(root.get("accountNumber"), accountId));
      Account account = session.createQuery(criteriaQuery).getSingleResult();
      return Optional.of(account);
    } catch (NoResultException e) {
      return Optional.empty();
    }
  }

  public Account saveAccount(Account accountToSave) {
    Session session = getCurrentSession();
    accountToSave = (Account) session.merge(accountToSave);
    session.getTransaction().commit();
    return accountToSave;
  }
}
