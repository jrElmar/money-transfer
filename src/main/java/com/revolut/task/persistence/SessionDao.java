package com.revolut.task.persistence;

import org.hibernate.Session;

abstract class SessionDao {

  Session openSession() {
    return SessionFactoryProvider.getSessionFactory()
        .openSession();
  }

  protected Session getCurrentSession() {
    return SessionFactoryProvider.getSessionFactory()
        .getCurrentSession();
  }
}
