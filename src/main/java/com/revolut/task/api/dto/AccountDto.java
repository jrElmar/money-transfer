package com.revolut.task.api.dto;

import com.revolut.task.model.Account;

import java.math.BigDecimal;

public class AccountDto {

  private long id;
  private String accountNumber;
  private BigDecimal balance;
  private String ownerFulName;

  public AccountDto(Account account) {
    this.id = account.getId();
    this.accountNumber = account.getAccountNumber();
    this.balance = account.getBalance();
    this.ownerFulName = account.getOwner().getName()
        .concat(" ")
        .concat(account.getOwner().getSurname());
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public String getOwnerFulName() {
    return ownerFulName;
  }

  public void setOwnerFulName(String ownerFulName) {
    this.ownerFulName = ownerFulName;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
