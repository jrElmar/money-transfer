package com.revolut.task.api.dto;

import java.math.BigDecimal;

public class AccountCreationRequest {

  private String accountId;
  private long customerId;
  private BigDecimal initialBalance;

  public AccountCreationRequest() {
  }

  public AccountCreationRequest(String accountId, long customerId, BigDecimal initialBalance) {
    this.accountId = accountId;
    this.customerId = customerId;
    this.initialBalance = initialBalance;
  }


  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(long customerId) {
    this.customerId = customerId;
  }

  public BigDecimal getInitialBalance() {
    return initialBalance;
  }

  public void setInitialBalance(BigDecimal initialBalance) {
    this.initialBalance = initialBalance;
  }
}
