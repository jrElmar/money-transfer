package com.revolut.task.api.dto;

import com.revolut.task.model.Customer;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class CustomerDto {

  private long id;
  private String fulName;
  private String creationDate;
  private List<AccountDto> accounts;

  public CustomerDto(Customer customer) {
    this.id = customer.getId();
    this.fulName = customer.getName()
        .concat(" ")
        .concat(customer.getSurname());
    this.creationDate = DateTimeFormatter.ISO_INSTANT.format(customer.getCreated());
    this.accounts = customer.getAccounts()
        .stream()
        .map(AccountDto::new)
        .collect(Collectors.toList());
  }

  public String getFulName() {
    return fulName;
  }

  public void setFulName(String fulName) {
    this.fulName = fulName;
  }

  public String getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public List<AccountDto> getAccounts() {
    return accounts;
  }

  public void setAccounts(List<AccountDto> accounts) {
    this.accounts = accounts;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
