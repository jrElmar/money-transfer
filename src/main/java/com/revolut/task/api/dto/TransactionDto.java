package com.revolut.task.api.dto;

import com.revolut.task.model.Transaction;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class TransactionDto {
  private long id;
  private String sourceId;
  private String destinationId;
  private BigDecimal amount;
  private String executionTime;

  public TransactionDto(Transaction transaction) {
    Objects.requireNonNull(transaction);
    this.id = transaction.getId();
    this.sourceId = transaction.getSource().getAccountNumber();
    this.destinationId = transaction.getDestination().getAccountNumber();
    this.amount = transaction.getAmount();
    this.executionTime = DateTimeFormatter.ISO_INSTANT
        .format(transaction.getCreated());
  }

  public TransactionDto() {
  }


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSourceId() {
    return sourceId;
  }

  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  public String getDestinationId() {
    return destinationId;
  }

  public void setDestinationId(String destinationId) {
    this.destinationId = destinationId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getExecutionTime() {
    return executionTime;
  }

  public void setExecutionTime(String executionTime) {
    this.executionTime = executionTime;
  }
}
