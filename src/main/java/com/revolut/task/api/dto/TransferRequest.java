package com.revolut.task.api.dto;

import java.math.BigDecimal;
import java.util.Objects;

public class TransferRequest {

  private String sourceAccount;
  private String destinationAccount;
  private BigDecimal amount;

  public TransferRequest(String sourceAccount, String destinationAccount, BigDecimal amount) {
    this.sourceAccount = sourceAccount;
    this.destinationAccount = destinationAccount;
    this.amount = amount;
  }

  public TransferRequest() {
  }

  public String getSourceAccount() {
    return sourceAccount;
  }

  public void setSourceAccount(String sourceAccount) {
    this.sourceAccount = sourceAccount;
  }

  public String getDestinationAccount() {
    return destinationAccount;
  }

  public void setDestinationAccount(String destinationAccount) {
    this.destinationAccount = destinationAccount;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  @Override
  public String toString() {
    return "TransferRequest{"
        + "sourceAccount='" + sourceAccount + '\''
        + ", destinationAccount='" + destinationAccount + '\''
        + ", amount=" + amount
        + '}';
  }

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    TransferRequest that = (TransferRequest) other;
    return Objects.equals(sourceAccount, that.sourceAccount)
        && Objects.equals(destinationAccount, that.destinationAccount)
        && Objects.equals(amount, that.amount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sourceAccount, destinationAccount, amount);
  }
}
