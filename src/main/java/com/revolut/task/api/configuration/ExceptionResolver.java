package com.revolut.task.api.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.task.exception.MoneyTransferException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class ExceptionResolver extends HttpServlet {


  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resolveError(req, resp);
  }

  private void resolveError(HttpServletRequest request,
                            HttpServletResponse response) throws IOException {
    ObjectMapper errorTransformer = new ObjectMapper();
    ErrorResponse errorResponse = null;

    Throwable throwable = (Throwable) request
        .getAttribute("javax.servlet.error.exception");

    if (throwable instanceof MoneyTransferException) {
      errorResponse = new ErrorResponse((MoneyTransferException) throwable);
    } else if (throwable instanceof RuntimeException) {
      errorResponse = new ErrorResponse(MoneyTransferException.DEFAULT_ERROR_CODE,
          (RuntimeException) throwable);
    }
    response.setContentType("application/json");
    response.getWriter()
        .write(errorTransformer.writeValueAsString(errorResponse));
  }

  public static final class ErrorResponse {

    private final int errorCode;
    private final String message;

    public ErrorResponse(int errorCode, RuntimeException runtime) {
      this.errorCode = errorCode;
      this.message = extractExceptionMessage(runtime.getMessage())
          .orElse(runtime.getClass().getName());
    }

    public ErrorResponse(MoneyTransferException moneyTransferException) {
      this.errorCode = moneyTransferException.getApplicationErrorCode();
      this.message = moneyTransferException.getMessage();
    }

    public int getErrorCode() {
      return errorCode;
    }


    public String getMessage() {
      return message;
    }


    private Optional<String> extractExceptionMessage(String message) {
      if (message == null || message.isEmpty()) {
        return Optional.empty();
      }
      int startingFrom = message.indexOf(":");
      startingFrom = startingFrom < 0 ? 0 : startingFrom + 1;
      return Optional.of(message.substring(startingFrom).trim());
    }

  }
}
