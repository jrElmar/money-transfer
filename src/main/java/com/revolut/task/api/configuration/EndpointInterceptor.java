package com.revolut.task.api.configuration;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public interface EndpointInterceptor {

  void preRequest(ServletRequest servletRequest, ServletResponse servletResponse);

  void postRequest(ServletRequest servletRequest, ServletResponse servletResponse);

}
