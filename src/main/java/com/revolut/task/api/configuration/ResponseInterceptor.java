package com.revolut.task.api.configuration;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;

public class ResponseInterceptor extends StandardMoneyTransferFilter {

  @Override
  public void postRequest(ServletRequest servletRequest, ServletResponse servletResponse) {
    HttpServletResponse response = (HttpServletResponse) servletResponse;
    response.setContentType("application/json");
    response.setCharacterEncoding(StandardCharsets.UTF_8.name());
  }
}
