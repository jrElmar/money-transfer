package com.revolut.task.api.configuration;

import com.revolut.task.api.endpoint.AccountCreationEndpoint;
import com.revolut.task.api.endpoint.AccountInformationEndpoint;
import com.revolut.task.api.endpoint.CustomerCreationEndpoint;
import com.revolut.task.api.endpoint.CustomerInformationEndpoint;
import com.revolut.task.api.endpoint.MoneyTransferEndpoint;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.ErrorPage;
import org.apache.tomcat.util.descriptor.web.FilterDef;
import org.apache.tomcat.util.descriptor.web.FilterMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationRunner {

  private String port;
  private String contextPath;
  private Tomcat tomcatInstance;

  public ApplicationRunner() throws IOException {
    Properties defaultConfigurations = loadAndGetDefaultServerConfiguration();
    this.contextPath = defaultConfigurations.getProperty("SERVER.CONTEXT.PATH.DEFAULT");
    this.port = defaultConfigurations.getProperty("SERVER.PORT.DEFAULT");
  }

  public ApplicationRunner withPort(String requestedPort) {
    if (requestedPort != null && !requestedPort.isEmpty()) {
      this.port = requestedPort;
    }
    return this;
  }

  public ApplicationRunner withContextPath(String requestedContextPath) {
    if (requestedContextPath != null && !requestedContextPath.isEmpty()) {
      this.contextPath = requestedContextPath;
    }
    return this;
  }

  public void start() throws LifecycleException {
    String webappDirLocation = ".";
    tomcatInstance = new Tomcat();
    tomcatInstance.setPort(Integer.parseInt(port));
    StandardContext context = (StandardContext) tomcatInstance.addContext(contextPath,
        new File(webappDirLocation).getAbsolutePath());

    registerServlets(context);
    tomcatInstance.start();
  }

  private void registerServlets(StandardContext context) {

    tomcatInstance.addServlet(contextPath,
        MoneyTransferEndpoint.class.getName(),
        MoneyTransferEndpoint.class.getName());
    context.addServletMapping("/transfer", MoneyTransferEndpoint.class.getName());

    tomcatInstance.addServlet(contextPath,
        AccountInformationEndpoint.class.getName(),
        AccountInformationEndpoint.class.getName());
    context.addServletMapping("/accounts", AccountInformationEndpoint.class.getName());

    tomcatInstance.addServlet(contextPath,
        AccountCreationEndpoint.class.getName(),
        AccountCreationEndpoint.class.getName());
    context.addServletMapping("/accounts/new", AccountCreationEndpoint.class.getName());

    tomcatInstance.addServlet(contextPath,
        CustomerCreationEndpoint.class.getName(),
        CustomerCreationEndpoint.class.getName());
    context.addServletMapping("/customers/new", CustomerCreationEndpoint.class.getName());

    tomcatInstance.addServlet(contextPath,
        CustomerInformationEndpoint.class.getName(),
        CustomerInformationEndpoint.class.getName());
    context.addServletMapping("/customers", CustomerInformationEndpoint.class.getName());

    tomcatInstance.addServlet(contextPath,
        ExceptionResolver.class.getName(),
        ExceptionResolver.class.getName());
    context.addServletMapping("/error", ExceptionResolver.class.getName());

    ErrorPage errorPage = new ErrorPage();
    errorPage.setLocation("/error");
    context.addErrorPage(errorPage);


    FilterDef responseFilterDefinition = new FilterDef();
    responseFilterDefinition.setFilterName(ResponseInterceptor.class.getSimpleName());
    responseFilterDefinition.setFilterClass(ResponseInterceptor.class.getName());
    context.addFilterDef(responseFilterDefinition);

    FilterMap responseFilterMapping = new FilterMap();
    responseFilterMapping.setFilterName(ResponseInterceptor.class.getSimpleName());
    responseFilterMapping.addURLPattern("/*");
    responseFilterMapping.setDispatcher("REQUEST");
    context.addFilterMap(responseFilterMapping);


    FilterDef hibernateFilterDefinition = new FilterDef();
    hibernateFilterDefinition.setFilterName(HibernateRequestInterceptor.class.getSimpleName());
    hibernateFilterDefinition.setFilterClass(HibernateRequestInterceptor.class.getName());
    context.addFilterDef(hibernateFilterDefinition);

    FilterMap hibernateFilterMapping = new FilterMap();
    hibernateFilterMapping.setFilterName(HibernateRequestInterceptor.class.getSimpleName());
    hibernateFilterMapping.addURLPattern("/*");
    hibernateFilterMapping.setDispatcher("REQUEST");
    context.addFilterMap(hibernateFilterMapping);

  }

  public void startAndWait() throws LifecycleException {
    start();
    tomcatInstance.getServer().await();
  }

  public void shutdown() throws LifecycleException {
    tomcatInstance.getServer().destroy();
  }

  private Properties loadAndGetDefaultServerConfiguration() throws IOException {
    InputStream configFile = getClass().getClassLoader()
        .getResourceAsStream("server.properties");
    Properties configurationAsPropertyFile = new Properties();
    configurationAsPropertyFile.load(configFile);
    return configurationAsPropertyFile;
  }

  public String getPort() {
    return port;
  }

  public String getContextPath() {
    return contextPath;
  }

}
