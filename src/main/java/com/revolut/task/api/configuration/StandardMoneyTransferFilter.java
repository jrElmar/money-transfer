package com.revolut.task.api.configuration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public abstract class StandardMoneyTransferFilter implements Filter, EndpointInterceptor {

  @Override
  public void preRequest(ServletRequest servletRequest, ServletResponse servletResponse) {

  }

  @Override
  public void postRequest(ServletRequest servletRequest, ServletResponse servletResponse) {

  }

  @Override
  public void doFilter(ServletRequest servletRequest,
                       ServletResponse servletResponse,
                       FilterChain filterChain) throws IOException, ServletException {

    preRequest(servletRequest, servletResponse);

    filterChain.doFilter(servletRequest, servletResponse);

    postRequest(servletRequest, servletResponse);
  }
}
