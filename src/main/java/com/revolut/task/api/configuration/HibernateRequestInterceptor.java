package com.revolut.task.api.configuration;

import com.revolut.task.persistence.SessionFactoryProvider;
import org.hibernate.Session;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


public class HibernateRequestInterceptor extends StandardMoneyTransferFilter {

  @Override
  public void preRequest(ServletRequest servletRequest, ServletResponse servletResponse) {
    try {
      SessionFactoryProvider.getSessionFactory()
          .getCurrentSession()
          .beginTransaction();
    } catch (RuntimeException e) {
      postRequest(servletRequest, servletResponse);
    }
  }

  @Override
  public void postRequest(ServletRequest servletRequest, ServletResponse servletResponse) {
    Session currentSession = SessionFactoryProvider.getSessionFactory()
        .getCurrentSession();
    try {
      currentSession.getTransaction().commit();
    } catch (RuntimeException e) {
      if (currentSession.getTransaction().isActive()) {
        currentSession.getTransaction().rollback();
      }
    }
  }
}
