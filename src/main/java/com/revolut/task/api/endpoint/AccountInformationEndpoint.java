package com.revolut.task.api.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.task.api.dto.AccountDto;
import com.revolut.task.bussiness.AccountService;
import com.revolut.task.bussiness.AccountServiceProvider;
import com.revolut.task.model.Account;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UncheckedIOException;

public class AccountInformationEndpoint extends HttpServlet {

  private final AccountService informationService;

  public AccountInformationEndpoint() {
    this.informationService = new AccountServiceProvider();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    try {
      String accId = req.getParameter("accNumber");

      ObjectMapper mapper = new ObjectMapper();
      Account account = informationService.getAccountDetails(accId);
      resp.setStatus(HttpServletResponse.SC_OK);
      resp.getWriter()
          .write(mapper.writeValueAsString(new AccountDto(account)));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
