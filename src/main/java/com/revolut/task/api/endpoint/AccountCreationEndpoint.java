package com.revolut.task.api.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.task.api.dto.AccountCreationRequest;
import com.revolut.task.api.dto.AccountDto;
import com.revolut.task.bussiness.AccountService;
import com.revolut.task.bussiness.AccountServiceProvider;
import com.revolut.task.model.Account;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;


public class AccountCreationEndpoint extends HttpServlet {

  private final AccountService accountService;

  public AccountCreationEndpoint() {
    this.accountService = new AccountServiceProvider();
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

    try (BufferedReader reader = req.getReader()) {
      StringBuilder bodyData = new StringBuilder();
      String currentLine;

      while ((currentLine = reader.readLine()) != null) {
        bodyData.append(currentLine);
      }

      ObjectMapper jsonTransformer = new ObjectMapper();
      AccountCreationRequest accountCreationRequest = jsonTransformer.readValue(bodyData.toString(),
          AccountCreationRequest.class);

      Account account = accountService.createAccount(
          accountCreationRequest.getAccountId(),
          accountCreationRequest.getCustomerId(),
          accountCreationRequest.getInitialBalance()
      );

      resp.setStatus(HttpServletResponse.SC_CREATED);
      resp.getWriter()
          .write(jsonTransformer.writeValueAsString(new AccountDto(account)));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }


}
