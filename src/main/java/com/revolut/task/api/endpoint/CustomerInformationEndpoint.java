package com.revolut.task.api.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.task.api.dto.CustomerDto;
import com.revolut.task.bussiness.CustomerService;
import com.revolut.task.bussiness.CustomerServiceProvider;
import com.revolut.task.model.Customer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UncheckedIOException;

public class CustomerInformationEndpoint extends HttpServlet {

  private final CustomerService customerService;

  public CustomerInformationEndpoint() {
    this.customerService = new CustomerServiceProvider();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
    try {
      long customerId = Long.parseLong(req.getParameter("customerId"));
      ObjectMapper mapper = new ObjectMapper();
      Customer customer = customerService.findById(customerId);

      resp.setStatus(HttpServletResponse.SC_OK);
      resp.getWriter()
          .write(mapper.writeValueAsString(new CustomerDto(customer)));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
