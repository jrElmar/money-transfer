package com.revolut.task.api.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.task.api.dto.CustomerCreationRequest;
import com.revolut.task.api.dto.CustomerDto;
import com.revolut.task.bussiness.CustomerService;
import com.revolut.task.bussiness.CustomerServiceProvider;
import com.revolut.task.model.Customer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;

public class CustomerCreationEndpoint extends HttpServlet {

  private final CustomerService customerService;

  public CustomerCreationEndpoint() {
    this.customerService = new CustomerServiceProvider();
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
    try (BufferedReader reader = req.getReader()) {
      StringBuilder bodyData = new StringBuilder();
      String currentLine;

      while ((currentLine = reader.readLine()) != null) {
        bodyData.append(currentLine);
      }

      ObjectMapper jsonTransformer = new ObjectMapper();
      CustomerCreationRequest creationRequest = jsonTransformer.readValue(bodyData.toString(),
          CustomerCreationRequest.class);


      Customer customer = customerService.registerCustomer(
          creationRequest.getName(),
          creationRequest.getSurname()
      );
      resp.setStatus(HttpServletResponse.SC_CREATED);
      resp.getWriter()
          .write(jsonTransformer.writeValueAsString(new CustomerDto(customer)));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
