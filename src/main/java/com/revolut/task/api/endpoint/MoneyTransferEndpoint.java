package com.revolut.task.api.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.task.api.dto.TransactionDto;
import com.revolut.task.api.dto.TransferRequest;
import com.revolut.task.bussiness.MoneyTransferService;
import com.revolut.task.bussiness.MoneyTransferServiceProvider;
import com.revolut.task.model.Transaction;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;

public class MoneyTransferEndpoint extends HttpServlet {

  private final MoneyTransferService moneyTransferService;

  public MoneyTransferEndpoint() {
    this.moneyTransferService = new MoneyTransferServiceProvider();
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
    try (BufferedReader reader = req.getReader()) {
      String line;
      StringBuilder bodyData = new StringBuilder();
      while ((line = reader.readLine()) != null) {
        bodyData.append(line);
      }
      ObjectMapper mapper = new ObjectMapper();

      TransferRequest transferRequest = mapper.readValue(bodyData.toString(),
          TransferRequest.class);


      Transaction transaction = moneyTransferService.askForTransfer(transferRequest.getAmount(),
          transferRequest.getSourceAccount(),
          transferRequest.getDestinationAccount());

      resp.setStatus(HttpServletResponse.SC_CREATED);
      resp.getWriter()
          .write(mapper.writeValueAsString(new TransactionDto(transaction)));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
