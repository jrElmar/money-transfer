package com.revolut.task;

import com.revolut.task.api.configuration.ApplicationRunner;
import org.apache.catalina.LifecycleException;

import java.io.IOException;


class App {
  public static void main(String[] args) {

    try {
      ApplicationRunner runner = new ApplicationRunner();

      if (args.length > 0) {
        runner = runner.withPort(args[0]);
      }

      if (args.length > 1) {
        runner = runner.withContextPath(args[1]);
      }

      runner.startAndWait();
    } catch (LifecycleException | IOException e) {
      e.printStackTrace(System.err);
    }
  }
}
