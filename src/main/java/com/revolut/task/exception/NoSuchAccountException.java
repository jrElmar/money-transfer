package com.revolut.task.exception;

public class NoSuchAccountException extends MoneyTransferException {

  public NoSuchAccountException() {
    super("there is no such account");
  }

  @Override
  public int getApplicationErrorCode() {
    return -1100;
  }
}
