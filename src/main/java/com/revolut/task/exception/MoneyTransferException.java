package com.revolut.task.exception;

public class MoneyTransferException extends RuntimeException {

  public static final int DEFAULT_ERROR_CODE = -1000;

  public MoneyTransferException() {
  }


  public MoneyTransferException(String message) {
    super(message);
  }


  public int getApplicationErrorCode() {
    return DEFAULT_ERROR_CODE;
  }
}
