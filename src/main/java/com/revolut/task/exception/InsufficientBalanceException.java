package com.revolut.task.exception;

public class InsufficientBalanceException extends MoneyTransferException {

  public InsufficientBalanceException() {
    super("balance is not sufficient");
  }

  @Override
  public int getApplicationErrorCode() {
    return -1200;
  }
}
