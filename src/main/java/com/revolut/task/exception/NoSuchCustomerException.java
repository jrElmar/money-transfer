package com.revolut.task.exception;

public class NoSuchCustomerException extends MoneyTransferException {

  public NoSuchCustomerException() {
    super("there is no such customer");
  }

  @Override
  public int getApplicationErrorCode() {
    return -1300;
  }
}
