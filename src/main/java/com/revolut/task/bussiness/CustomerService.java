package com.revolut.task.bussiness;

import com.revolut.task.model.Customer;

public interface CustomerService {

  Customer findById(long id);

  Customer registerCustomer(String name, String surname);

}
