package com.revolut.task.bussiness;

import com.revolut.task.exception.NoSuchAccountException;
import com.revolut.task.model.Account;
import com.revolut.task.model.Customer;
import com.revolut.task.persistence.AccountDao;

import java.math.BigDecimal;

public class AccountServiceProvider implements AccountService {

  private final AccountDao accountDao;
  private final CustomerService customerService;

  public AccountServiceProvider() {
    this(new AccountDao(), new CustomerServiceProvider());
  }

  public AccountServiceProvider(AccountDao accountDao, CustomerService customerService) {
    this.accountDao = accountDao;
    this.customerService = customerService;
  }

  @Override
  public Account getAccountDetails(String accountId) {
    return accountDao.findByAccountId(accountId)
        .orElseThrow(NoSuchAccountException::new);
  }

  @Override
  public Account createAccount(String accountId, long customerId, BigDecimal initialBalance) {

    if (accountId == null || accountId.isEmpty()) {
      throw new IllegalArgumentException("unacceptable account number was provided");
    }
    if (initialBalance == null || initialBalance.compareTo(BigDecimal.ZERO) < 0) {
      initialBalance = BigDecimal.ZERO;
    }

    Customer customer = customerService.findById(customerId);

    Account account = new Account();
    account.setAccountNumber(accountId);
    account.setBalance(initialBalance);
    account.setOwner(customer);
    return accountDao.saveAccount(account);
  }
}
