package com.revolut.task.bussiness;

import com.revolut.task.exception.InsufficientBalanceException;
import com.revolut.task.exception.MoneyTransferException;
import com.revolut.task.model.Account;
import com.revolut.task.model.Transaction;
import com.revolut.task.persistence.TransferDao;
import org.apache.tomcat.util.collections.ManagedConcurrentWeakHashMap;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;

public class MoneyTransferServiceProvider implements MoneyTransferService {

  private static final Map<String, Object> locks = new ManagedConcurrentWeakHashMap<>();

  private final TransferDao transferDao;
  private final AccountService accountService;

  public MoneyTransferServiceProvider() {
    this(new TransferDao(), new AccountServiceProvider());
  }

  public MoneyTransferServiceProvider(TransferDao transferDao,
                                      AccountService accountService) {
    this.transferDao = transferDao;
    this.accountService = accountService;
  }

  @Override
  public Transaction askForTransfer(BigDecimal amount,
                                    String sourceAccountId,
                                    String destinationAccountId) {

    if (amount == null || sourceAccountId == null || destinationAccountId == null) {
      throw new IllegalArgumentException("all transfer parameters should be provided");
    }
    if (sourceAccountId.equals(destinationAccountId)) {
      throw new MoneyTransferException("you can not transfer money within same account");
    }
    if (amount.compareTo(BigDecimal.ZERO) <= 0) {
      throw new MoneyTransferException("amount should be a positive number");
    }

    locks.putIfAbsent(sourceAccountId, new Object());
    locks.putIfAbsent(destinationAccountId, new Object());

    int comparisonResult = sourceAccountId.compareTo(destinationAccountId);
    Transaction transaction;
    if (comparisonResult > 0) {
      synchronized (locks.get(sourceAccountId)) {
        synchronized (locks.get(destinationAccountId)) {
          transaction = transfer(amount, sourceAccountId, destinationAccountId);
        }
      }
    } else {
      synchronized (locks.get(destinationAccountId)) {
        synchronized (locks.get(sourceAccountId)) {
          transaction = transfer(amount, sourceAccountId, destinationAccountId);
        }
      }
    }
    return transaction;
  }

  private Transaction transfer(BigDecimal amount,
                               String sourceAccountId,
                               String destinationAccountId) {

    Account source = accountService.getAccountDetails(sourceAccountId);
    Account destination = accountService.getAccountDetails(destinationAccountId);

    if (source.getBalance().compareTo(amount) < 0) {
      throw new InsufficientBalanceException();
    }

    source.setBalance(source.getBalance().subtract(amount));
    destination.setBalance(destination.getBalance().add(amount));

    Transaction transaction = new Transaction();
    transaction.setAmount(amount);
    transaction.setSource(source);
    transaction.setDestination(destination);
    transaction.setCreated(Instant.now());
    transaction = transferDao.saveTransaction(transaction);

    ((ManagedConcurrentWeakHashMap) locks).maintain();
    return transaction;
  }


}
