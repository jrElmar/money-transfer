package com.revolut.task.bussiness;

import com.revolut.task.exception.NoSuchCustomerException;
import com.revolut.task.model.Customer;
import com.revolut.task.persistence.CustomerDao;

import java.time.Instant;

public class CustomerServiceProvider implements CustomerService {

  private final CustomerDao customerDao;

  public CustomerServiceProvider(CustomerDao customerDao) {
    this.customerDao = customerDao;
  }

  public CustomerServiceProvider() {
    this(new CustomerDao());
  }

  @Override
  public Customer findById(long id) {
    return customerDao.findCustomerById(id)
        .orElseThrow(NoSuchCustomerException::new);
  }

  @Override
  public Customer registerCustomer(String name, String surname) {
    if (name == null || name.isEmpty() || surname == null || surname.isEmpty()) {
      throw new IllegalArgumentException("name and surname should be provided");
    }
    Customer customer = new Customer();
    customer.setName(name);
    customer.setSurname(surname);
    customer.setCreated(Instant.now());
    return customerDao.saveCustomer(customer);
  }
}

