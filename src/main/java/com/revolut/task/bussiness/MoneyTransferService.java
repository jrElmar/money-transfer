package com.revolut.task.bussiness;

import com.revolut.task.model.Transaction;

import java.math.BigDecimal;

public interface MoneyTransferService {

  Transaction askForTransfer(BigDecimal amount,
                             String sourceAccountId,
                             String destinationAccountId);
}
