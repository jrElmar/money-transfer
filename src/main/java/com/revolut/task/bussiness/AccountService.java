package com.revolut.task.bussiness;

import com.revolut.task.model.Account;

import java.math.BigDecimal;

public interface AccountService {

  Account getAccountDetails(String accountId);

  Account createAccount(String accountId, long customerId, BigDecimal initialBalance);
}
