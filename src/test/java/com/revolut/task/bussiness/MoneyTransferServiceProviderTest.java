package com.revolut.task.bussiness;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.revolut.task.api.dto.TransferRequest;
import com.revolut.task.exception.MoneyTransferException;
import com.revolut.task.model.Account;
import com.revolut.task.model.Transaction;
import com.revolut.task.persistence.TransferDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class MoneyTransferServiceProviderTest {

  @InjectMocks
  private MoneyTransferServiceProvider moneyTransferService;
  @Mock
  private TransferDao transferDao;
  @Mock
  private AccountService accountService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  void testWhenMultipleSimultaneouslyTransferComing() throws InterruptedException {
    int threads = 20;
    CountDownLatch transferBlocker = new CountDownLatch(1);
    CountDownLatch testBodyBlocker = new CountDownLatch(threads);

    Account destination = new Account();
    destination.setBalance(new BigDecimal("1000"));
    destination.setAccountNumber("destinationId");

    when(accountService.getAccountDetails("destinationId"))
        .then(invocationOnMock -> {
          Thread.sleep(100);
          return destination;
        });

    for (int i = 1; i <= threads; i++) {
      Account sourceX = new Account();
      sourceX.setBalance(new BigDecimal("1500"));
      sourceX.setAccountNumber("source" + i);
      when(accountService.getAccountDetails(sourceX.getAccountNumber()))
          .then(invocationOnMock -> {
            Thread.sleep(100);
            return sourceX;
          });
    }


    ExecutorService service = Executors.newFixedThreadPool(threads);

    for (int i = 1; i <= threads; i++) {
      TransferRequest transferRequest = new TransferRequest();
      transferRequest.setAmount(new BigDecimal("15.5"));
      transferRequest.setSourceAccount("source" + i);
      transferRequest.setDestinationAccount(destination.getAccountNumber());
      service.submit(prepareAndGetTransferRequest(transferRequest, testBodyBlocker, transferBlocker));
    }

    transferBlocker.countDown();
    testBodyBlocker.await();
    service.shutdown();

    verify(transferDao, times(threads))
        .saveTransaction(any(Transaction.class));

    assertThat(destination.getBalance())
        .isEqualByComparingTo(new BigDecimal("1310"));
  }


  @Test
  void transferSafeShouldThrowExceptionWhenAccountsAreSame() {
    String sourceAccountId = "123456ABCDFG";
    String destinationAccountId = "123456ABCDFG";

    assertThatThrownBy(() ->
        moneyTransferService.askForTransfer(BigDecimal.TEN, sourceAccountId, destinationAccountId))
        .isInstanceOf(MoneyTransferException.class)
        .hasMessage("you can not transfer money within same account");
  }

  @Test
  void transferSafeShouldThrowExceptionWithNullParameters() {
    String sourceAccountId = "123456ABCDFG";
    String destinationAccountId = "123456ABCDFG";

    assertThatThrownBy(() ->
        moneyTransferService.askForTransfer(null, sourceAccountId, destinationAccountId))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("all transfer parameters should be provided");

    assertThatThrownBy(() ->
        moneyTransferService.askForTransfer(BigDecimal.TEN, null, destinationAccountId))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("all transfer parameters should be provided");

    assertThatThrownBy(() ->
        moneyTransferService.askForTransfer(BigDecimal.TEN, sourceAccountId, null))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("all transfer parameters should be provided");
  }

  @Test
  void transferSafeShouldThrowExceptionWithNonPositiveAmount() {
    String sourceAccountId = "123456ABCDFG";
    String destinationAccountId = "123456ABCDFGK";

    assertThatThrownBy(() ->
        moneyTransferService.askForTransfer(new BigDecimal("-10"), sourceAccountId, destinationAccountId))
        .isInstanceOf(MoneyTransferException.class)
        .hasMessage("amount should be a positive number");

  }


  @Test
  void transferSafeShouldThrowExceptionWhenBalanceIsInsufficient() {
    String sourceAccountId = "123456ABCDFG";
    String destinationAccountId = "123456ABCDFGK";
    Account source = new Account();
    source.setAccountNumber(sourceAccountId);
    source.setBalance(new BigDecimal("150"));
    source.setId(1);

    Account destination = new Account();
    destination.setId(2);
    destination.setAccountNumber(destinationAccountId);
    destination.setBalance(new BigDecimal("10"));

    when(accountService.getAccountDetails(sourceAccountId))
        .thenReturn(source);
    when(accountService.getAccountDetails(destinationAccountId))
        .thenReturn(destination);

    assertThatThrownBy(() ->
        moneyTransferService.askForTransfer(new BigDecimal("500"), sourceAccountId, destinationAccountId))
        .isInstanceOf(MoneyTransferException.class)
        .hasMessage("balance is not sufficient");

    assertThat(source.getBalance()).isEqualByComparingTo(new BigDecimal("150"));
    assertThat(destination.getBalance()).isEqualByComparingTo(new BigDecimal("10"));
    verify(transferDao, times(0)).saveTransaction(any(Transaction.class));
  }

  private Runnable prepareAndGetTransferRequest(TransferRequest transferRequest,
                                                CountDownLatch testBodyBlocker,
                                                CountDownLatch transferBlocker) {
    return () -> {
      try {
        transferBlocker.await();
        moneyTransferService.askForTransfer(transferRequest.getAmount(),
            transferRequest.getSourceAccount(),
            transferRequest.getDestinationAccount());
        testBodyBlocker.countDown();
      } catch (InterruptedException e) {
        e.printStackTrace(System.err);
      }
    };
  }
}