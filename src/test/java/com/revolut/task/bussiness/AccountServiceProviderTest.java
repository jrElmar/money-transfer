package com.revolut.task.bussiness;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.math.BigDecimal;
import java.util.Optional;
import com.revolut.task.exception.NoSuchAccountException;
import com.revolut.task.model.Account;
import com.revolut.task.model.Customer;
import com.revolut.task.persistence.AccountDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class AccountServiceProviderTest {


  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @InjectMocks
  private AccountServiceProvider accountServiceProvider;
  @Mock
  private AccountDao accountDao;
  @Mock
  private CustomerService customerService;

  @Test
  void getAccountDetailsShouldReturnAccount() {
    String accountId = "12345ABCDE";

    Account expectedAccount = new Account();
    expectedAccount.setId(1);
    expectedAccount.setAccountNumber(accountId);
    expectedAccount.setBalance(new BigDecimal("100"));

    when(accountDao.findByAccountId(accountId))
        .thenReturn(Optional.of(expectedAccount));

    Account actualAccount = accountServiceProvider.getAccountDetails(accountId);
    verify(accountDao, times(1)).findByAccountId(accountId);
    assertThat(actualAccount).isEqualTo(expectedAccount);
  }

  @Test
  void getAccountDetailsShouldThrowException() {
    String accountId = "12345ABCDE";
    when(accountDao.findByAccountId(accountId))
        .thenReturn(Optional.empty());

    assertThatThrownBy(() -> accountServiceProvider.getAccountDetails(accountId))
        .isInstanceOf(NoSuchAccountException.class)
        .hasMessage("there is no such account");
  }

  @Test
  void testCreateAccountPositiveCase() {

    Customer customer = new Customer();
    customer.setId(1);
    customer.setName("Peter");
    customer.setSurname("Parker");

    Account accountFromDB = new Account();
    accountFromDB.setAccountNumber("testId");
    accountFromDB.setBalance(BigDecimal.TEN);
    accountFromDB.setOwner(customer);
    accountFromDB.setId(1);

    when(accountDao.saveAccount(any(Account.class)))
        .thenReturn(accountFromDB);

    when(customerService.findById(1))
        .thenReturn(customer);

    Account actualAccount = accountServiceProvider.createAccount("testId",
        1,
        BigDecimal.TEN);

    verify(accountDao, times(1))
        .saveAccount(any(Account.class));
    verify(customerService, times(1))
        .findById(1L);

    assertThat(actualAccount)
        .extracting("id",
            "accountNumber",
            "balance",
            "owner.name",
            "owner.surname")
        .containsExactly(1L, "testId", BigDecimal.TEN, "Peter", "Parker");
  }


  @Test
  void testAccountCreationWithNegativeInitialBalance() {

    Customer customer = new Customer();
    customer.setId(1);
    customer.setName("Peter");
    customer.setSurname("Parker");

    Account accountFromDB = new Account();
    accountFromDB.setAccountNumber("testId");
    accountFromDB.setBalance(new BigDecimal("-1"));
    accountFromDB.setOwner(customer);
    accountFromDB.setId(1);

    ArgumentCaptor<Account> accountPassed = ArgumentCaptor.forClass(Account.class);

    when(accountDao.saveAccount(any(Account.class)))
        .thenReturn(accountFromDB);

    when(customerService.findById(1))
        .thenReturn(customer);

    accountServiceProvider.createAccount("testId",
        1L,
        new BigDecimal("-1"));

    verify(accountDao, times(1))
        .saveAccount(any(Account.class));

    verify(accountDao).saveAccount(accountPassed.capture());

    verify(customerService, times(1))
        .findById(1L);

    assertThat(accountPassed.getValue().getBalance())
        .isEqualByComparingTo(BigDecimal.ZERO);

  }
}