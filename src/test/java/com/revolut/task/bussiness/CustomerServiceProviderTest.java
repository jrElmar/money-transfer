package com.revolut.task.bussiness;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.time.Instant;
import java.util.Optional;
import com.revolut.task.exception.NoSuchCustomerException;
import com.revolut.task.model.Customer;
import com.revolut.task.persistence.CustomerDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class CustomerServiceProviderTest {

  @InjectMocks
  private CustomerServiceProvider customerServiceProvider;
  @Mock
  private CustomerDao customerDao;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
  }


  @Test
  void testFindByIdWhenIdDoesNotExist() {
    when(customerDao.findCustomerById(1))
        .thenReturn(Optional.empty());

    assertThatThrownBy(() -> customerServiceProvider.findById(1))
        .isInstanceOf(NoSuchCustomerException.class)
        .hasMessage("there is no such customer");
  }

  @Test
  void testFindByIdPositive() {
    Customer expected = new Customer();
    when(customerDao.findCustomerById(1))
        .thenReturn(Optional.of(expected));
    Customer actual = customerServiceProvider.findById(1);
    assertThat(actual).isNotNull();
    verify(customerDao, times(1))
        .findCustomerById(1);
  }

  @Test
  void testCreationWithIncorrectParams() {
    assertThatThrownBy(() -> customerServiceProvider.registerCustomer(null, null))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("name and surname should be provided");
    assertThatThrownBy(() -> customerServiceProvider.registerCustomer(null, ""))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("name and surname should be provided");
    assertThatThrownBy(() -> customerServiceProvider.registerCustomer("", ""))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("name and surname should be provided");
    assertThatThrownBy(() -> customerServiceProvider.registerCustomer("", null))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("name and surname should be provided");
  }

  @Test
  void testCustomerCreation() {
    String name = "Peter";
    String surname = "Parker";

    Customer fromDao = new Customer();
    fromDao.setSurname(surname);
    fromDao.setName(name);
    fromDao.setId(1);
    fromDao.setCreated(Instant.now());

    when(customerDao.saveCustomer(any(Customer.class)))
        .thenReturn(fromDao);

    ArgumentCaptor<Customer> customerPassed = ArgumentCaptor.forClass(Customer.class);
    Customer actual = customerServiceProvider.registerCustomer(name, surname);

    verify(customerDao, times(1))
        .saveCustomer(any(Customer.class));
    verify(customerDao).saveCustomer(customerPassed.capture());

    assertThat(customerPassed.getValue())
        .extracting("name", "surname")
        .containsExactly(name, surname);

    assertThat(actual)
        .extracting("name", "surname", "id")
        .containsExactly(name, surname, 1L);

    assertThat(actual.getAccounts()).isEmpty();
    assertThat(actual.getCreated()).isNotNull();
  }
}