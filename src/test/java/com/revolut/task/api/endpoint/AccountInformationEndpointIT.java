package com.revolut.task.api.endpoint;


import static org.hamcrest.Matchers.is;
import com.revolut.task.EndpointBaseIT;
import io.restassured.RestAssured;
import org.apache.catalina.LifecycleException;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountInformationEndpointIT extends EndpointBaseIT {

  @BeforeAll
  static void startServer() throws InterruptedException {
    setUpServer();
  }

  @AfterAll
  static void stopServer() throws LifecycleException {
    tearDownServer();
  }
  @BeforeEach
  void setUp() {
    cleanupDB();
    initializeTestData();
  }

  @Test
  void testRetrieval() {
    RestAssured.given()
        .accept("application/json")
        .param("accNumber", "account4")
        .when()
        .get(getRoot() + "/accounts")
        .then()
        .assertThat()
        .statusCode(HttpStatus.SC_OK)
        .body("id", is(4))
        .body("accountNumber", is("account4"))
        .body("ownerFulName", is("Bruce Wayne"))
        .body("balance", is(40000F));
  }
}