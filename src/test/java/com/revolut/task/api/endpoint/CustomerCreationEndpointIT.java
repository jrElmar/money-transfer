package com.revolut.task.api.endpoint;


import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import com.revolut.task.EndpointBaseIT;
import com.revolut.task.api.dto.CustomerCreationRequest;
import io.restassured.RestAssured;
import org.apache.catalina.LifecycleException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class CustomerCreationEndpointIT extends EndpointBaseIT {

  @BeforeAll
  static void startServer() throws InterruptedException {
    setUpServer();
  }

  @AfterAll
  static void stopServer() throws LifecycleException {
    tearDownServer();
  }

  @Test
  void testCreationPositiveCase() {
    String name = "Robert";
    String surname = "Downey JR";
    CustomerCreationRequest creationRequest = new CustomerCreationRequest(name, surname);

    RestAssured.given()
        .accept("application/json")
        .body(creationRequest)
        .when()
        .post(getRoot() + "/customers/new")
        .then()
        .assertThat()
        .statusCode(201)
        .body("fulName", is(name + " " + surname))
        .body("creationDate", notNullValue());
  }
}