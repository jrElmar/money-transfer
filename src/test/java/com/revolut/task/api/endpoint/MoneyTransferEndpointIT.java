package com.revolut.task.api.endpoint;

import static com.revolut.task.api.configuration.ExceptionResolver.ErrorResponse;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import java.math.BigDecimal;
import java.util.stream.Stream;
import com.revolut.task.EndpointBaseIT;
import com.revolut.task.api.dto.TransferRequest;
import com.revolut.task.exception.InsufficientBalanceException;
import com.revolut.task.exception.MoneyTransferException;
import io.restassured.RestAssured;
import org.apache.catalina.LifecycleException;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class MoneyTransferEndpointIT extends EndpointBaseIT {

  @BeforeAll
  static void startServer() throws InterruptedException {
    setUpServer();
  }

  @AfterAll
  static void stopServer() throws LifecycleException {
    tearDownServer();
  }

  @BeforeEach
  void setUp() {
    cleanupDB();
    initializeTestData();
  }

  @ParameterizedTest(name = "{index}-{0}")
  @MethodSource("provideIncorrectRequest")
  void testTransferWithIncorrectRequest(String description,
                                        TransferRequest transferRequest,
                                        ErrorResponse expectedErrorResponse) {
    RestAssured.given()
        .accept("application/json")
        .body(transferRequest)
        .when()
        .post(getRoot() + "/transfer")
        .then()
        .assertThat()
        .statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
        .body("errorCode", is(expectedErrorResponse.getErrorCode()))
        .body("message", is(expectedErrorResponse.getMessage()));
  }

  private static Stream<Arguments> provideIncorrectRequest() {
    return Stream.of(
        Arguments.of(
            "when the accounts are same",
            new TransferRequest("account1", "account1", new BigDecimal("10")),
            new ErrorResponse(new MoneyTransferException("you can not transfer money within same account"))
        ),
        Arguments.of(
            "when the amount is not positive",
            new TransferRequest("account1", "account2", new BigDecimal("-1")),
            new ErrorResponse(new MoneyTransferException("amount should be a positive number"))
        ),
        Arguments.of(
            "when source account is not provided",
            new TransferRequest(null, "account2", BigDecimal.TEN),
            new ErrorResponse(MoneyTransferException.DEFAULT_ERROR_CODE,
                new IllegalArgumentException("all transfer parameters should be provided"))
        ),
        Arguments.of(
            "when the balance is not sufficient",
            new TransferRequest("account1", "account2", new BigDecimal("10000000")),
            new ErrorResponse(new InsufficientBalanceException())
        )
    );
  }

  @Test
  void testTransferOnPositiveCase() {
    TransferRequest transferRequest = new TransferRequest();
    transferRequest.setAmount(new BigDecimal("10.5"));
    transferRequest.setSourceAccount("account1");
    transferRequest.setDestinationAccount("account2");
    RestAssured.given()
        .accept("application/json")
        .body(transferRequest)
        .when()
        .post(getRoot() + "/transfer")
        .then()
        .assertThat()
        .statusCode(HttpStatus.SC_CREATED)
        .body("id", notNullValue())
        .body("sourceId", equalTo("account1"))
        .body("destinationId", equalTo("account2"))
        .body("amount", is(10.5F))
        .body("executionTime", notNullValue());

  }

}