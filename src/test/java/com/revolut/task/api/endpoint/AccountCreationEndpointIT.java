package com.revolut.task.api.endpoint;

import static org.hamcrest.Matchers.is;
import java.math.BigDecimal;
import com.revolut.task.EndpointBaseIT;
import com.revolut.task.api.dto.AccountCreationRequest;
import io.restassured.RestAssured;
import org.apache.catalina.LifecycleException;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountCreationEndpointIT extends EndpointBaseIT {


  @BeforeAll
  static void startServer() throws InterruptedException {
    setUpServer();
  }

  @AfterAll
  static void stopServer() throws LifecycleException {
    tearDownServer();
  }

  @BeforeEach
  void setUp() {
    cleanupDB();
    initializeTestData();
  }

  @Test
  void testCreationOfAccountPositiveCase() {

    AccountCreationRequest creationRequest = new AccountCreationRequest();
    creationRequest.setAccountId("testAccountId");
    creationRequest.setCustomerId(3);
    creationRequest.setInitialBalance(BigDecimal.ZERO);

    RestAssured.given()
        .accept("application/json")
        .body(creationRequest)
        .when()
        .post(getRoot() + "/accounts/new")
        .then()
        .assertThat()
        .statusCode(HttpStatus.SC_CREATED)
        .body("accountNumber", is("testAccountId"))
        .body("balance", is(0))
        .body("ownerFulName", is("Peter Parker"));
  }
}