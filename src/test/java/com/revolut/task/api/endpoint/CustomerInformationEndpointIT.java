package com.revolut.task.api.endpoint;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import com.revolut.task.EndpointBaseIT;
import io.restassured.RestAssured;
import org.apache.catalina.LifecycleException;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CustomerInformationEndpointIT extends EndpointBaseIT {

  @BeforeAll
  static void startServer() throws InterruptedException {
    setUpServer();
  }

  @AfterAll
  static void stopServer() throws LifecycleException {
    tearDownServer();
  }

  @BeforeEach
  void setUp() {
    cleanupDB();
    initializeTestData();
  }

  @Test
  void testCustomerInformation() {
    RestAssured.given()
        .accept("application/json")
        .param("customerId", 4L)
        .when()
        .get(getRoot() + "/customers")
        .then()
        .assertThat()
        .statusCode(HttpStatus.SC_OK)
        .body("id", is(4))
        .body("fulName", is("Bruce Wayne"))
        .body("creationDate", notNullValue())
        .body("accounts", hasSize(2))
        .body("accounts[0]", notNullValue())
        .body("accounts[1]", notNullValue())
        .body("accounts[0].accountNumber", is("account4"))
        .body("accounts[1].accountNumber", is("account5"));
  }
}