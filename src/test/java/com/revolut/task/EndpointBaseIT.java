package com.revolut.task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import com.revolut.task.api.configuration.ApplicationRunner;
import com.revolut.task.persistence.SessionFactoryProvider;
import org.apache.catalina.LifecycleException;
import org.hibernate.Session;

public class EndpointBaseIT {

  private static ApplicationRunner applicationRunner;
  private static final CountDownLatch testBlocker = new CountDownLatch(1);

  protected static void setUpServer() throws InterruptedException {

    try {
      applicationRunner = new ApplicationRunner();
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      applicationRunner.start();
    } catch (LifecycleException e) {
      e.printStackTrace();
    }
    testBlocker.countDown();
    testBlocker.await();
  }

  protected static void tearDownServer() throws LifecycleException {
    try {
      if (applicationRunner != null) {
        applicationRunner.shutdown();
      }
    } catch (Exception e) {

    }
  }

  protected String getRoot() {
    String port = applicationRunner.getPort();
    String contextPath = applicationRunner.getContextPath();
    String root = "http://localhost:";
    return root.concat(port).concat(contextPath);
  }


  protected void initializeTestData() {
    InputStream dataAsStream = EndpointBaseIT.class.getResourceAsStream("/sql/init-data.sql");
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(dataAsStream))) {
      StringBuilder scripts = new StringBuilder();
      String currentLine;
      while ((currentLine = reader.readLine()) != null) {
        if (!currentLine.startsWith("--")) {
          scripts.append("\n").append(currentLine);
        }
      }
      Session session = SessionFactoryProvider.getSessionFactory()
          .openSession();
      session.beginTransaction();
      session.createNativeQuery(scripts.toString())
          .executeUpdate();
      session.getTransaction().commit();
      session.close();
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  protected void cleanupDB() {
    Session session = SessionFactoryProvider.getSessionFactory().openSession();
    session.beginTransaction();
    Arrays.asList("TRANSACTIONS", "ACCOUNTS", "CUSTOMERS")
        .forEach(tableName -> session.createNativeQuery("delete from " + tableName)
            .executeUpdate());
    session.getTransaction().commit();
    session.close();
  }


}
